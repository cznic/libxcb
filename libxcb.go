// Copyright 2024 The libXdmcp-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libxcb is a ccgo/v4 version of libxcb.a, a library implementing the
// client-side of the X11 display server protocol.
package libxcb // import "modernc.org/libxcb"
